<?php

namespace App\Controllers;

use App\Models\TTemp;
use CodeIgniter\API\ResponseTrait;

class Post extends BaseController
{
    use ResponseTrait;

    public function Save()
    {
        // Get data from API
        $url = "https://portal2.incoe.astra.co.id/vendor_rating/api/Approve_json";
        $client = \Config\Services::curlrequest();
        $response = $client->request('GET', $url);
        $data = json_decode($response->getBody(), true);

        // Insert data to database using tData model
        $model = new TTemp();
        foreach ($data as $item) {
            $model->save([
                'no_dn' => $item['no_dn'],
                'no_po' => $item['no_po'],
                'nama_supplier' => $item['nama_supplier'],
                'date_delivery_plan' => $item['date_delivery_plan'],
                'date_done' => $item['date_done'],
                'qa_date' => $item['qa_date'],
                'warehouse_security' => $item['warehouse_security'],
                'receipt_security' => $item['receipt_security'],
                'kendaraan_polisi' => $item['kendaraan_polisi'],
                'driver' => $item['driver'],
                'receipt_standard' => $item['receipt_standard']
            ]);
        }
        return redirect()->to('/');
    }
}

