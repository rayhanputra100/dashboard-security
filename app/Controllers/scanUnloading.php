<?php

namespace App\Controllers;

use App\Models\TScanUnloading;

class scanUnloading extends BaseController
{
    protected $scanUnloadingModel;
    public function __construct()
    {
        $this->scanUnloadingModel = new TScanUnloading();
    }

    public function sendScanUnloading()
    {
        $postDN = strtoupper($this->request->getVar('noDNUnloading'));
        $existingData = $this->scanUnloadingModel->where('noDN', $postDN)->first();

        $data = [
            'noDN' => $postDN,
            'status' => $postDN ? 'True' : 'False',
        ];

        if ($this->scanUnloadingModel->save($data)) {
            if ($existingData) {
                session()->setFlashdata('warning', 'Data Sudah Ada');
            } else {
                session()->setFlashdata('success', 'Data Berhasil Di Input');
            }
        } else {
            session()->setFlashdata('danger', 'Data Gagal Di Input');
        }

        return redirect()->to(base_url('/unloading'));
    }

    public function scanUnloadingView()
    {
        return view('pages/scanDNUnloading');
    }
}
