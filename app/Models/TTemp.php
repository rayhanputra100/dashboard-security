<?php

namespace App\Models;

use CodeIgniter\Model;

class TTemp extends Model
{
    // protected $table            = 'incoming_temp_data';
    protected $table            = 'TTemp';
    protected $allowedFields    = ['no_dn', 'no_po', 'date_delivery_plan','confirm_delivery_plan','date_done','qa_date','warehouse_date','receipt_security','kendaraan_polisi','driver','nama_supplier','URGENT','receipt_standard'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}
