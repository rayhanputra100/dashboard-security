<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dashboard" />
    <meta content="PT CBI || Rayhan Putra" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Supplier Incoming Control & Monitoring</title>

    <!-- CSS -->
    <link rel="stylesheet" type="" href="<?= base_url(); ?>mycss/style.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- Boostrap CSS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/iPTCBI.ico">

    <!-- DataTables -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive Datatable -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Plugins css -->
    <link href="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.css" rel="stylesheet" />
    <link href="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
</head>


<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <?= $this->include('/layouts/navbar'); ?>
    <?= $this->renderSection('content'); ?>

    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2023 PT CBI.
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->


    <!-- jQuery  -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waves.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Plugins js -->
    <script src="<?= base_url(); ?>assets/plugins/timepicker/moment.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/clockpicker/jquery-clockpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColor.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asGradient.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColorPicker.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>

    <script src="<?= base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

    <!-- Plugins Init js -->
    <script src="<?= base_url(); ?>assets/pages/form-advanced.js"></script>
    <!-- Datatable init js -->
    <script src="<?= base_url(); ?>assets/js/tableHTMLExport.js"></script>


    <!-- App js -->
    <script src="<?= base_url(); ?>assets/js/app.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable();
            // $(".export-btn").click(function() {

            //     $("#datatable").tableHTMLExport({
            //         type: 'csv',
            //         filename: "Data Supplier Incoming Control & Monitoring(<?= $dateNow ?>).csv",
            //     });

            // });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.bs-example-modal-lg').on('shown.bs.modal', function() {
                $('#noDN').focus();
            });
        });
    </script>
    <script>
        setTimeout(function() {
            location.reload();
        }, 5 * 60 * 1000); // Refresh halaman setiap 5 menit (5 * 60 * 1000 milidetik)
    </script>
    <script>
        $(document).ready(function() {
            var formattedDatas = <?php echo json_encode($formattedDatas); ?>;

            $('#pickSup').on('change', function() {
                var selectedOption = $(this).val();
                var namaSup = '';
                var truckData = [];
                var uniquePlatNos = [];

                if (selectedOption !== '') {
                    formattedDatas.forEach(function(data) {
                        if (data.nama_supplier === selectedOption) {
                            namaSup = data.nama_supplier;

                            var jadwal = data.tPlanIn;
                            if (!uniquePlatNos.includes(jadwal)) {
                                uniquePlatNos.push(jadwal);
                            }

                            truckData.push({
                                noDN: data.noDN,
                                dSecIn: data.dSecIn,
                                dQC: data.dQC,
                                dWHC: data.dWHC,
                                dSecOut: data.dSecOut,
                                tSecIn: data.tSecIn,
                                tPlanIn: data.tPlanIn,
                                tQC: data.tQC,
                                tWHC: data.tWHC,
                                tSecOut: data.tSecOut,
                                max: data.max,
                                min: data.min,
                                past: data.past,
                                dnScan: data.dnScan,
                                status: data.status,
                                tScanQCR: data.tScanQCR,
                                platNo: data.platNo
                            });
                        }
                    });

                    // Memasukkan nilai ke dalam elemen HTML
                    $('#namaSup').text(namaSup);
                    $('#jumlahTruck').text(uniquePlatNos.length);

                    // Menampilkan jadwal truck
                    var $jadwalElement = $('#jadwalTruck');
                    $jadwalElement.empty();

                    if (uniquePlatNos.length > 1) {
                        var jadwalText = uniquePlatNos.join(' - ');
                        $jadwalElement.text(jadwalText);
                    } else if (uniquePlatNos.length === 1) {
                        $jadwalElement.text(uniquePlatNos[0]);
                    } else {
                        $jadwalElement.text("");
                    }

                    // Menampilkan data truck dalam tabel
                    var $dataBody = $('#dataBody');
                    $dataBody.empty();

                    truckData.forEach(function(data) {
                        var $row = $('<tr>');
                        var $noDN = $('<td>').addClass('bordered text-center px-3').text(data.noDN);
                        var $planIn = $('<td>').addClass('bordered text-center px-3').text(data.tPlanIn);
                        var $secIn = $('<td>').addClass('bordered text-center px-3');
                        var $qcr = $('<td>').addClass('bordered text-center px-3');
                        var $qc = $('<td>').addClass('bordered text-center px-3');
                        var $whc = $('<td>').addClass('bordered text-center px-3');
                        var $secOut = $('<td>').addClass('bordered text-center px-3');

                        if (data.dSecIn == '01-01-1980' || data.dSecIn == data.past) {
                            $secIn.addClass('text-center').text('-');
                        } else {
                            $secIn.addClass('text-center ' + (data.tSecIn < data.max && data.tSecIn > data.min ? 'colorGreen' : 'colorRed')).text(data.tSecIn);
                        }

                        if (data.noDN !== data.dnScan || data.status === 'false') {
                            $qcr.addClass('text-center').text('-');
                        } else {
                            $qcr.addClass('text-center colorGreen').text(data.tScanQCR);
                        }

                        if (data.dQC == '01-01-1980' || data.dQC == data.past) {
                            $qc.addClass('text-center').text('-');
                        } else {
                            $qc.addClass('text-center ' + (data.tQC > data.tSecIn && data.tQC < data.tWHC && data.tQC < data.tSecOut && data.tQC > data.tScanQCR ? 'colorGreen' : 'colorRed')).text(data.tQC);
                        }

                        if (data.tWHC == '07:00' || data.dWHC == data.past) {
                            $whc.addClass('text-center').text('-');
                        } else {
                            $whc.addClass('text-center ' + (data.tWHC > data.tSecIn && data.tWHC > data.tQC && data.tWHC < data.tSecOut ? 'colorGreen' : 'colorRed')).text(data.tWHC);
                        }

                        if (data.dSecOut == '01-01-1980' || data.dSecOut == data.past) {
                            $secOut.addClass('text-center').text('-');
                        } else {
                            $secOut.addClass('text-center ' + (data.tSecOut > data.tQC && data.tSecOut > data.tWHC && data.tSecOut > data.tQC ? 'colorGreen' : 'colorRed')).text(data.tSecOut);
                        }

                        $row.append($noDN, $planIn, $secIn, $qcr, $qc, $whc, $secOut);
                        $dataBody.append($row);
                    });

                    // Menampilkan modal saat ada data yang ditampilkan
                    $('#dataModal').modal('show');
                } else {
                    // Menutup modal jika tidak ada data yang dipilih
                    $('#dataModal').modal('hide');
                }
            });
        });
    </script>

</body>

</html>