<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dashboard" />
    <meta content="PT CBI || Rayhan Putra" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Supplier Incoming Control & Monitoring</title>

    <!-- CSS -->
    <link rel="stylesheet" type="" href="<?= base_url(); ?>mycss/style.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- Boostrap CSS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/iPTCBI.ico">

    <!-- DataTables -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive Datatable -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Plugins css -->
    <link href="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.css" rel="stylesheet" />
    <link href="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
</head>


<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <?= $this->include('layouts/navbar.php'); ?>
    <?= $this->renderSection('content'); ?>

    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2023 PT CBI.
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
    <!-- Hightchart -->
    <script src="<?php echo base_url(); ?>assets/code/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/export-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/accessibility.js"></script>
    <!-- jQuery  -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waves.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Plugins js -->
    <script src="<?= base_url(); ?>assets/plugins/timepicker/moment.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/clockpicker/jquery-clockpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColor.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asGradient.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColorPicker.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>

    <script src="<?= base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

    <!-- Plugins Init js -->
    <script src="<?= base_url(); ?>assets/pages/form-advanced.js"></script>
    <!-- Datatable init js -->
    <script src="<?= base_url(); ?>assets/js/tableHTMLExport.js"></script>

    <!-- App js -->
    <script src="<?= base_url(); ?>assets/js/app.js"></script>

    <script>
        setTimeout(function() {
            location.reload();
        }, 5 * 60 * 1000); // Refresh halaman setiap 5 menit (5 * 60 * 1000 milidetik)
    </script>
    <script type="text/javascript">
        Highcharts.chart('containerSchedule', {

            chart: {
                type: "column",
                backgroundColor: "white",
                width: 1100, // Atur lebar chart sesuai kebutuhan
                height: 270, // Atur tinggi chart sesuai kebutuhan
            },
            title: {
                text: "SUPPLIER SEC IN TIME",
                style: {
                    color: "black", // Ganti dengan warna teks yang diinginkan
                    textOutline: "none" // Menghilangkan border pada teks
                },
            },
            yAxis: [{
                labels: {
                    style: {
                        color: "black", // Ganti dengan warna teks yang diinginkan
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }],

            xAxis: [{
                categories: [
                    <?php foreach ($formattedDatasTruck as $v) { ?> "<?php echo $v['nama_supplier']; ?>",
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "black", // Ganti dengan warna teks yang diinginkan
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }, ],

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: [{
                    type: 'spline',
                    name: 'Supplier',
                    data: [
                        <?php foreach ($formattedDatasTruck as $v) { ?> <?php echo intval($v['timeDiff']); ?>,
                        <?php } ?>
                    ],
                    marker: {
                        states: {
                            hover: {
                                fillColor: this.y < 0 && this.y > 45 ? 'red' : null,
                            }
                        }
                    }
                },
                {
                    type: 'spline',
                    name: 'Target',
                    data: [<?php foreach ($formattedDatasTruck as $v) { ?> <?php echo intval(60); ?>,
                        <?php } ?>
                    ],
                    color: 'red',
                }
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerTruck", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
                width: 1100, // Atur lebar chart sesuai kebutuhan
                height: 270, // Atur tinggi chart sesuai kebutuhan
            },
            title: {
                text: "QTY TRUCK IN AREA",
                align: "center",
            },

            yAxis: {
                min: 0,
                title: {
                    text: "Truck ",
                },
                stackLabels: {
                    enabled: true,
                },
            },

            xAxis: [{
                categories: ["07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00"],
                labels: {
                    style: {
                        color: "black", // Ganti dengan warna teks yang diinginkan
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }, ],

            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "Truck Plan",
                    data: [
                        <?php foreach ($hourlyDataPlan as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                },
                {
                    name: 'Truck Aktual',
                    stack: 1,
                    data: [
                        <?php foreach ($hourlyData as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                },
                {
                    type: 'spline',
                    name: 'Line Truck',
                    data: [
                        <?php foreach ($hourlyData as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                },
                {
                    type: 'spline',
                    name: 'Target',
                    data: [<?php foreach ($hourlyData as $v) { ?> <?php echo intval(5); ?>,
                        <?php } ?>
                    ],
                    color: 'red',
                }
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerTime", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
                width: 1100, // Atur lebar chart sesuai kebutuhan
                height: 270, // Atur tinggi chart sesuai kebutuhan
            },
            title: {
                text: "HIGHEST RANGE TIME",
                align: "center",
            },

            yAxis: {
                min: 0,
                title: {
                    text: "Total Range Time",
                },
                stackLabels: {
                    enabled: true,
                },
            },

            xAxis: [{
                categories: [
                    <?php foreach ($filterRangeTimeTruck as $v) : ?> '<?php echo $v['nama_supplier']; ?> (<?php echo $v['platNo']; ?>)',
                    <?php endforeach; ?>
                ],

                labels: {
                    style: {
                        color: "black",
                        textOutline: "none"
                    },
                }
            }],


            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "Range Time",
                    data: [
                        <?php foreach ($filterRangeTimeTruck as $v) { ?>
                            <?php echo floatval($v['timeDiff']); ?>,
                        <?php } ?>
                    ],
                },
                {
                    type: 'spline',
                    name: 'Line Range Time',
                    data: [<?php foreach ($filterRangeTimeTruck as $v) { ?>
                            <?php echo floatval($v['timeDiff']); ?>,
                        <?php } ?>
                    ],
                },
                {
                    type: 'spline',
                    name: 'Target',
                    data: [<?php foreach ($filterRangeTimeTruck as $v) { ?> <?php echo intval(45); ?>,
                        <?php } ?>
                    ],
                    color: 'red',
                }
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerAveTime", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
                width: 1100, // Atur lebar chart sesuai kebutuhan
                height: 270, // Atur tinggi chart sesuai kebutuhan
            },
            title: {
                text: "AVERAGE RANGE TIME",
                align: "center",
            },

            yAxis: {
                min: 0,
                title: {
                    text: "Total Range Time",
                },
                stackLabels: {
                    enabled: true,
                },
            },

            xAxis: [{
                categories: [
                    <?php foreach ($averageResults as $v) { ?> '<?php echo $v['nama_supplier']; ?>',
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "black",
                        textOutline: "none"
                    },
                }
            }],


            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "Range Time",
                    data: [
                        <?php foreach ($averageResults as $v) { ?>
                            <?php echo floatval($v['averageTimeDiff']); ?>,
                        <?php } ?>
                    ],
                    color: 'yellow'
                },
                {
                    type: 'spline',
                    name: 'Line Range Time',
                    data: [<?php foreach ($averageResults as $v) { ?>
                            <?php echo floatval($v['averageTimeDiff']); ?>,
                        <?php } ?>
                    ],
                },
                {
                    type: 'spline',
                    name: 'Target',
                    data: [<?php foreach ($averageResults as $v) { ?> <?php echo intval(45); ?>,
                        <?php } ?>
                    ],
                    color: 'red',
                }
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerNGSup", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
                width: 1100, // Atur lebar chart sesuai kebutuhan
                height: 270, // Atur tinggi chart sesuai kebutuhan
            },
            title: {
                text: "DAILY DELAY MONITORING PLAN IN SUPPLIER",
                align: "center",
            },

            yAxis: {
                min: 0,
                title: {
                    text: "Total NG",
                },
                stackLabels: {
                    enabled: true,
                },
            },

            xAxis: [{
                categories: [
                    <?php foreach ($NGDatas as $supplierName => $data) { ?> '<?php echo $supplierName; ?>',
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "black",
                        textOutline: "none"
                    },
                }
            }],


            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "Total NG",
                    data: [
                        <?php foreach ($NGDatas as $supplierName => $data) { ?>
                            <?php echo intval($data); ?>,
                        <?php } ?>
                    ],
                    color: 'orange'
                },
                {
                    type: 'spline',
                    name: 'Line Total NG',
                    data: [<?php foreach ($NGDatas as $supplierName => $data) { ?>
                            <?php echo intval($data); ?>,
                        <?php } ?>
                    ],
                }
            ],
        });
    </script>


</body>

</html>