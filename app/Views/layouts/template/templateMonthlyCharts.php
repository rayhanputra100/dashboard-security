<?php 
// dd($detailNGDatasYear); ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dashboard" />
    <meta content="PT CBI || Rayhan Putra" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Supplier Incoming Control & Monitoring</title>

    <!-- CSS -->
    <link rel="stylesheet" type="" href="<?= base_url(); ?>mycss/style.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- Boostrap CSS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/iPTCBI.ico">

    <!-- DataTables -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive Datatable -->
    <link href="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Plugins css -->
    <link href="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.css" rel="stylesheet" />
    <link href="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
</head>


<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <?= $this->include('layouts/navbar.php'); ?>
    <?= $this->renderSection('content'); ?>

    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2023 PT CBI.
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
    <!-- Hightchart -->
    <script src="<?php echo base_url(); ?>assets/code/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/export-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/code/modules/accessibility.js"></script>
    <!-- jQuery  -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waves.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Plugins js -->
    <script src="<?= base_url(); ?>assets/plugins/timepicker/moment.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/tempusdominus-bootstrap-4.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/clockpicker/jquery-clockpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColor.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asGradient.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/colorpicker/jquery-asColorPicker.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>

    <script src="<?= base_url(); ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

    <!-- Plugins Init js -->
    <script src="<?= base_url(); ?>assets/pages/form-advanced.js"></script>
    <!-- Datatable init js -->
    <script src="<?= base_url(); ?>assets/js/tableHTMLExport.js"></script>

    <!-- App js -->
    <script src="<?= base_url(); ?>assets/js/app.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var formattedDatas = <?php echo json_encode($formattedDatas); ?>;
            var chart = Highcharts.chart("containerNGSup", {
                chart: {
                    type: "column",
                    backgroundColor: "transparent",
                    width: 1100, // Atur lebar chart sesuai kebutuhan
                    height: 270, // Atur tinggi chart sesuai kebutuhan
                },
                title: {
                    text: "DAILY MONITORING DELAY PLAN IN SUPPLIER",
                    align: "center",
                },
                // Rest of your Highcharts options here...
                yAxis: {
                    title: {
                        text: "Total Range Time",
                    },
                },

                xAxis: [{
                    categories: [
                        <?php foreach ($combinedNGDatas as $v) { ?> '<?php echo $v['date']; ?>',
                        <?php } ?>
                    ],
                    labels: {
                        style: {
                            color: "black",
                            textOutline: "none"
                        },
                    }
                }],

                legend: {
                    align: "left",
                    x: 70,
                    verticalAlign: "top",
                    y: 30,
                    floating: true,
                    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                    borderColor: "#CCC",
                    borderWidth: 1,
                    shadow: false,
                },
                tooltip: {
                    headerFormat: "<b>{point.x}</b><br/>",
                    pointFormat: "{series.name}: {point.y}",
                },
                plotOptions: {
                    column: {
                        stacking: "normal",
                        dataLabels: {
                            enabled: true,
                        },
                    },
                },
                series: [{
                    id: 'totalNGSeries',
                    name: "Total NG",
                    data: [
                        <?php foreach ($combinedNGDatas as $v) { ?>
                            <?php echo intval($v['count']); ?>,
                        <?php } ?>
                    ],
                    color: 'orange',
                    point: {
                        events: {
                            click: function(event) {
                                // Get the index of the clicked point
                                var pointIndex = event.point.index;

                                // Get the data for the clicked point from the combinedNGDatas array
                                var data = <?php echo json_encode($combinedNGDatas); ?>;
                                var clickedData = data[pointIndex]['details'];

                                // Extract the required data (nama_supplier, tPlanIn, tSecIn, platNo) from the clickedData array
                                var extractedData = [];
                                for (var i = 0; i < clickedData.length; i++) {
                                    var rowData = clickedData[i];
                                    extractedData.push({
                                        'date': rowData['date'],
                                        'nama_supplier': rowData['nama_supplier'],
                                        'platNo': rowData['platNo'],
                                        'tPlanIn': rowData['tPlanIn'],
                                        'tSecIn': rowData['tSecIn'],
                                        'max': rowData['max'],
                                        'min': rowData['min'],
                                        'timeDiffSecIn': rowData['timeDiffSecIn']
                                    });
                                }

                                // Update the modal content with the extracted data
                                var modalBody = $('#dataBody');
                                modalBody.empty();

                                for (var i = 0; i < extractedData.length; i++) {
                                    var rowData = extractedData[i];
                                    var colorClass = rowData['tSecIn'] <= rowData['max'] && rowData['tSecIn'] >= rowData['min'] ? 'colorGreen' : 'colorRed';
                                    var row = '<tr>' +
                                        '<td class="text-center px-3 bordered">' + (i + 1) + '</td>' +
                                        '<td class="text-center px-3 bordered">' + rowData['nama_supplier'] + '</td>' +
                                        '<td class="text-center px-3 bordered">' + rowData['platNo'] + '</td>' +
                                        '<td class="text-center px-3 bordered">' + rowData['tPlanIn'] + '</td>' +
                                        '<td class="text-center px-3 bordered ' + colorClass + '">' + rowData['tSecIn'] + '</td>' +
                                        '<td class="text-center px-3 bordered">' + rowData['timeDiffSecIn'] + '</td>' +
                                        '</tr>';
                                    modalBody.append(row);
                                }


                                // Show the modal
                                $('#dataModal').modal('show');
                            }
                        }
                    }
                }, {
                    type: 'spline',
                    name: 'Line Total NG',
                    data: [<?php foreach ($combinedNGDatas as $v) { ?>
                            <?php echo intval($v['count']); ?>,
                        <?php } ?>
                    ],
                    point: {
                        events: {
                            click: function(event) {
                                // Same click event as the column series
                                this.series.chart.series[0].options.point.events.click.call(this, event);
                            }
                        }
                    }
                }],
            });
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerNGYear", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
                width: 1100,
                height: 270,
            },
            title: {
                text: "MONTHLY MONITORING DELAY PLAN IN SUPPLIER",
                align: "center",
            },
            xAxis: {
                categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                labels: {
                    style: {
                        color: "black",
                        textOutline: "none"
                    },
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: "Count trophies",
                },
                stackLabels: {
                    enabled: true,
                },
            },

            legend: {
                enabled: false
            },

            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [
                <?php
                $rowCounter = 0;
                foreach ($detailNGDatasYear[$rowCounter]['details'] as $supplier => $count) {
                ?> {
                        name: "<?php echo $supplier; ?>",
                        data: [
                            <?php
                            foreach ($detailNGDatasYear as $row) {
                                $supplierCount = $row['details'][$supplier] ?? 0;
                                echo $supplierCount . ",";
                            }
                            ?>
                        ],
                    },
                <?php
                }
                $rowCounter++;
                ?>
            ],
        });
    </script>
</body>

</html>