<?= $this->extend('layouts/template/templateCharts'); ?>

<?= $this->section('content'); ?>

<?php

// dd($averageResults);
?>
<!-- Wrapper -->
<div class="wrapper mt-3">

    <!-- Container -->
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title fs-4">Dashboard Charts Incoming</h4>
                </div>
            </div>
        </div>
        <!-- End Page-Title -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="row" style="margin: 10px 0 0 10px;">
                        <div class="col-2">
                            <form action="<?= site_url('daily_charts') ?>" method="get">
                                <label for="filter" class="form-label mb-1">Filter</label>
                                <input type="text" class="form-control mb-2" name="date" placeholder="<?= $date == null ? date('Y/m/d') : $filterDate = date('Y/m/d', strtotime($date)); ?>" id="mdate">
                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-filter"></i> Filter</button>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerSchedule"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerTruck"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerTime"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerAveTime"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerNGSup"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>