USE [DB_SUP]
GO
/****** Object:  Table [dbo].[TTemp]    Script Date: 7/28/2023 8:17:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TTemp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no_dn] [varchar](50) NULL,
	[no_po] [varchar](50) NULL,
	[date_delivery_plan] [varchar](50) NULL,
	[confirm_delivery_plan] [varchar](50) NULL,
	[date_done] [varchar](50) NULL,
	[qa_date] [varchar](50) NULL,
	[warehouse_date] [varchar](50) NULL,
	[receipt_security] [varchar](50) NULL,
	[kendaraan_polisi] [varchar](50) NULL,
	[driver] [varchar](50) NULL,
	[nama_supplier] [varchar](50) NULL,
	[URGENT] [varchar](50) NULL,
	[receipt_standard] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_TTemp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TTemp] ADD  CONSTRAINT [DF_TTemp_created_at]  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[TTemp] ADD  CONSTRAINT [DF_TTemp_updated_at]  DEFAULT (getdate()) FOR [updated_at]
GO
